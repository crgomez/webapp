import { NgModule } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';

import { ItemService } from './services/item.service';


@NgModule({
    declarations: [],
    imports: [
        NativeScriptCommonModule
    ],
    exports: [],
    providers: [
        ItemService
    ],
})
export class CoreModule { }
