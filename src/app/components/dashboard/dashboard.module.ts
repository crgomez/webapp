import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';

import { DashboardComponent } from './components/dashboard/dashboard.compoent';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    declarations: [DashboardComponent],
    imports: [
        NativeScriptCommonModule,
        DashboardRoutingModule,
        SharedModule
    ],
    exports: [],
    schemas: [NO_ERRORS_SCHEMA]
})
export class NameModule { }
