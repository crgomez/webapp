import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RouterExtensions } from '@nativescript/angular';
import { Page } from '@nativescript/core';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

    actionBarTitle: String = 'Iniciar sesión';
    isLogin: Boolean = false;
    textLoader: String = 'Iniciando Sesión';
    formGroup: FormGroup;

    constructor(
        private page: Page,
        private routerExtensions: RouterExtensions,
        private formBuilder: FormBuilder
    ) { }

    ngOnInit(): void {
        this.startBuildForm();
    }

    logIn(): void {
        this.isLogin = true;
        setTimeout(() => {
            this.isLogin = false;
            this.page.actionBarHidden = false;
        }, 1500);
    }

    // Método que carga el formGroup con todas las funcionalidades integradas.
    startBuildForm(): void {
        this.formGroup = this.formBuilder.group({
            email: ['', [Validators.required, Validators.pattern('^[^@]+@[^@]+\.[a-zA-Z]{2,}$')]],
            password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(32)]]
        });
    }

    // Propiedades get para acceder a las propiedades del formGroup de una manera más sencilla
    // Estas propiedades para este caso se usan para obtener los errores que tengan para mostrarlos al usuario en el pantalla
    get emailField() {
        return this.formGroup.get('email');
    }

    get passwordField() {
        return this.formGroup.get('password');
    }
}