import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "@nativescript/angular";

import { Item } from "../../../../core/models/item";
import { ItemService } from "../../../../core/services/item.service";

@Component({
    selector: "ns-details",
    templateUrl: "./item-detail.component.html",
    styleUrls: ['./item-detail.component.css']
})
export class ItemDetailComponent implements OnInit {
    item: Item;
    id: number;

    constructor(
        private itemService: ItemService,
        private activateRoute: ActivatedRoute,
        private routerExtensions: RouterExtensions
    ) {}

    ngOnInit(): void {
        const id = +this.activateRoute.snapshot.params.id;
        this.item = this.itemService.getItem(id);
    }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }
}
