import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "@nativescript/angular";

import { Item } from "../../../../core/models/item";
import { ItemService } from "../../../../core/services/item.service";

@Component({
    selector: "ns-items",
    templateUrl: "./items.component.html",
    styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
    items: Array<Item>;

    constructor(
        private itemService: ItemService,
        private routerExtensions: RouterExtensions
    ) { }

    ngOnInit(): void {
        this.items = this.itemService.getItems();
    }

    onItemTap(ItemId: number) {
        this.routerExtensions.navigate(['/items/detail', ItemId], {
            transition: {
                name: 'fade'
            }
        })
    }

}
