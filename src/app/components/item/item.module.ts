import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';

import { ItemDetailComponent } from './components/item-detail/item-detail.component';
import { ItemsComponent } from './components/items/items.component';

import { ItemRoutingModule } from './item-routing.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    declarations: [
        ItemDetailComponent,
        ItemsComponent
    ],
    imports: [
        NativeScriptCommonModule,
        ItemRoutingModule,
        SharedModule
    ],
    exports: [],
    schemas: [NO_ERRORS_SCHEMA]
})
export class ItemModule { }
